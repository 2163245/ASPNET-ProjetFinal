﻿//Auteur: Jean-Francois Gamache
namespace GestionHoraire.Models
{
    public class BDLocalRepository : ILocalRepository
    {
        //Initalisé le DBContext
        private readonly GestionHoraireDBContext _DBContext;
        public BDLocalRepository(GestionHoraireDBContext dBContext)
        {
            _DBContext = dBContext;
        }

        //Méthodes
        public List<Locaux> ListeLocaux
        {
            get => _DBContext.Locaux.OrderBy(l => l.Id).ToList();
        }

        /// <summary>
        /// Fonction qui permet d'ajouter
        /// </summary>
        /// <param name="local"></param>
        public void Ajouter(Locaux local)
        {
            _DBContext.Locaux.Add(local);
            _DBContext.SaveChanges();
        }

        /// <summary>
        /// Fonction qui modifie le local donné
        /// </summary>
        /// <param name="local"></param>
        public void Modifier(Locaux local)
        {
            _DBContext.Locaux.Update(local);
            _DBContext.SaveChanges();
        }

        /// <summary>
        /// Trouver le cours équivalent à l'ID et le supprimer de la base de donné
        /// </summary>
        /// <param name="id"></param>
        public void Supprimer(int id)
        {
            Locaux local = _DBContext.Locaux.FirstOrDefault(l => l.Id == id);
            if (local != null)
            {
                _DBContext.Locaux.Remove(local);
                _DBContext.SaveChanges();
            }
        }
        /// <summary>
        /// Fonction qui récupère un local par son ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Locaux GetLocal(int id)
        {
            Locaux local = _DBContext.Locaux.FirstOrDefault(c => c.Id == id);
            return local;
        }
    }
}
