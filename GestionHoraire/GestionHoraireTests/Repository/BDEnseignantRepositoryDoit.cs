﻿//Author : Samir El Haddaji
using GestionHoraire.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionHoraireTests.Repository
{
    public class BDEnseignantRepositoryDoit
    {
        // Initialisation des variables globales pour la classe de test
        private DbContextOptions<GestionHoraireDBContext> _options;
        private GestionHoraireDBContext _dbContext;
        private BDEnseignantRepository _enseignantRepository;

        public BDEnseignantRepositoryDoit()
        {
            // Configuration des options pour la base de données en mémoire
            _options = new DbContextOptionsBuilder<GestionHoraireDBContext>()
                .UseInMemoryDatabase("GestionHoraireDBContextTest")
                .Options;

            _dbContext = new GestionHoraireDBContext(_options);
            _enseignantRepository = new BDEnseignantRepository(_dbContext);
        }

        // Test de la création d'un nouvel enseignant
        [Fact]
        public void TesterAjouterEnseignant()
        {
            var enseignant = new Enseignants { Id = 1, Prenom = "test", Nom = "test", Bureau = "1.075", Poste = 2325 };

            // Ajout de enseignant via le repository
            _enseignantRepository.Ajouter(enseignant);

            // Vérification que l'enseignant a bien été ajouté
            Assert.NotEmpty(_enseignantRepository.ListeEnsignant);
            Assert.Equal(1, _enseignantRepository.ListeEnsignant.Count);
            Assert.Contains(_enseignantRepository.ListeEnsignant, e => e.Id == 1);
        }

        // Test de la modification d'un nouvel enseignant
        [Fact]
        public void TesterModifierEnseignants()
        {
            var enseignant = new Enseignants { Id = 2, Prenom = "test2", Nom = "test2", Bureau = "1.075", Poste = 2325 };

            // Ajout de l'enseignant directement dans le contexte de la base de données
            _dbContext.Enseignant.Add(enseignant);
            _dbContext.SaveChanges();

            // Modification d'une propriété de l'enseignant
            enseignant.Prenom = "EnseignantModifié";

            // Mise à jour de l'enseignant via le repository
            _enseignantRepository.Modifier(enseignant);

            // Vérification que l'enseignant a bien été modifié
            Assert.Equal("EnseignantModifié", _enseignantRepository.GetEnseignant(2).Prenom);
        }

        // Test de la suppression d'un nouvel enseignant
        [Fact]
        public void TesterSupprimerEnseignants()
        {
            var enseignant = new Enseignants { Id = 3, Prenom = "test3", Nom = "test3", Bureau = "1.075", Poste = 2325 };

            // Ajout de l'enseignant directement dans le contexte de la base de données
            _dbContext.Enseignant.Add(enseignant);
            _dbContext.SaveChanges();

            // Suppression de l'enseignant via le repository
            _enseignantRepository.Supprimer(3);

            // Vérification que l'enseignant a bien été supprimé
            Assert.Null(_enseignantRepository.GetEnseignant(3));
        }
    }
}
