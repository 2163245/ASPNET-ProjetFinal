﻿//Author: Samir El Haddaji

namespace GestionHoraire.Models
{
    public interface IEnseignantRepository
    {
        public List<Enseignants> ListeEnsignant { get; }

        public void Ajouter(Enseignants enseignant);

        public void Modifier(Enseignants enseignant);

        public void Supprimer(int id);

        public Enseignants GetEnseignant(int id);
    }
}
