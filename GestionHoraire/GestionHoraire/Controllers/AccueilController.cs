﻿using GestionHoraire.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace GestionHoraire.Controllers
{
    public class AccueilController : Controller
    {
        public ViewResult Index()
        {
            //Cookies pour le compte du nombre de cours dans le panier
            int compteur = 0;
            if (this.Request.Cookies["nbCours"] != null)
                compteur = Convert.ToInt16(this.Request.Cookies["nbCours"]);
            else
                this.Response.Cookies.Append("nbCours", compteur.ToString());

            ViewBag.Compteur = compteur;

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }
    }
}