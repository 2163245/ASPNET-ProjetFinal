using GestionHoraire.Models;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using GestionHoraire.Models;

var builder = WebApplication.CreateBuilder(args);
var connectionString = builder.Configuration.GetConnectionString("GestionHoraireDBContextConnection") ?? throw new InvalidOperationException("Connection string 'GestionHoraireDBContextConnection' not found.");

builder.Services.AddControllersWithViews();

//Attribution des Repository
builder.Services.AddScoped<IClientRepository, BDClientRepository>();
builder.Services.AddScoped<ICoursRepository, BDCoursRepository>();
builder.Services.AddScoped<IEnseignantRepository, BDEnseignantRepository>();
builder.Services.AddScoped<ILocalRepository, BDLocalRepository>();
builder.Services.AddScoped<IPanierRepository, BDPanierRepository>();

//Initialisation de la base de donn�es et connection
builder.Services.AddDbContext<GestionHoraireDBContext>(options =>
{
    options.UseSqlServer(connectionString);
});

builder.Services.AddIdentity<IdentityUser, IdentityRole>(options => options.SignIn.RequireConfirmedAccount = false)
    .AddEntityFrameworkStores<GestionHoraireDBContext>().AddRoles<IdentityRole>().AddDefaultUI().AddDefaultTokenProviders();

var app = builder.Build();

//Gestion des pages d'erreurs
if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}
else
{
    app.UseStatusCodePagesWithReExecute("/Erreurs/Status{0}");
    app.UseExceptionHandler("/ErreurGlobal");
}

app.UseStaticFiles();

//Authentification et autorisation
app.UseAuthentication();
app.UseAuthorization();
app.MapRazorPages();


app.MapControllerRoute(
 name: "default",
 pattern: "{controller=Accueil}/{action=index}"
);

//Seed pour valeurs par defaults
InitialisateurBD.Seed(app);

app.Run();
