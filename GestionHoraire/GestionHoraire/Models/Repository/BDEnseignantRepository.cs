﻿//Auteur: Samir El Haddaji

namespace GestionHoraire.Models
{
    public class BDEnseignantRepository : IEnseignantRepository
    {
        //Initalisé le DBContext
        private readonly GestionHoraireDBContext _gestionHoraireDBContext;
        public BDEnseignantRepository(GestionHoraireDBContext gestionHoraireDBContext)
        {
            _gestionHoraireDBContext = gestionHoraireDBContext;
        }

        public List<Enseignants> ListeEnsignant
        {
            get
            {
                return _gestionHoraireDBContext.Enseignant.OrderBy(c => c.Nom).ToList();
            }
        }

        //Méthodes
        public void Ajouter(Enseignants enseignant)
        {
            _gestionHoraireDBContext.Enseignant.Add(enseignant);
            _gestionHoraireDBContext.SaveChanges();
        }

        public Enseignants GetEnseignant(int id)
        {
            Enseignants enseignant = _gestionHoraireDBContext.Enseignant.FirstOrDefault(e => e.Id == id);
            return enseignant;
        }

        public void Modifier(Enseignants enseignant)
        {
            _gestionHoraireDBContext.Enseignant.Update(enseignant);
            _gestionHoraireDBContext.SaveChanges();
        }

        /// <summary>
        /// Trouver le cours équivalent à l'ID et le supprimer de la base de donné
        /// </summary>
        /// <param name="id"></param>
        public void Supprimer(int id)
        {
            Enseignants enseignant = _gestionHoraireDBContext.Enseignant.FirstOrDefault(e => e.Id == id);
            if (enseignant != null)
            {
                _gestionHoraireDBContext.Enseignant.Remove(enseignant);
                _gestionHoraireDBContext.SaveChanges();
            }
        }
    }
}
