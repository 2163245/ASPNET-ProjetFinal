﻿//Auteur: Jean-Francois Gamache
using Microsoft.AspNetCore.Mvc;
using GestionHoraire.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authorization;

namespace GestionHoraire.Controllers
{
    [Authorize]
    public class LocalController : Controller
    {
        private ILocalRepository _localRepository;
        public LocalController(ILocalRepository localRepository)
        {
            _localRepository = localRepository;
        }

        /// <summary>
        /// Fonction qui apporte à la liste des locaux
        /// </summary>
        /// <returns></returns>
        public ViewResult Liste()
        {
            ViewData["Title"] = "Liste des locaux";
            return View(_localRepository.ListeLocaux);
        }

        /// <summary>
        /// Fonction qui permet permet de récupérer les détails du locaux choisi par ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ViewResult Details(int id)
        {
            Locaux local = _localRepository.GetLocal(id);
            return View(local);
        }

        /// <summary>
        /// Fonction qui apporte à la vue pour ajouter un cours
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ViewResult Ajouter()
        {
            return View();
        }

        /// <summary>
        /// Fonction qui permet d'ajouter un local
        /// </summary>
        /// <param name="local"></param>
        /// <returns></returns>
        [HttpPost]
        public RedirectToActionResult Ajouter(Locaux local)
        {
            _localRepository.Ajouter(local);
            return RedirectToAction(nameof(Liste));
        }

        /// <summary>
        /// Fonction qui permet d'apporter a la page de modification
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ViewResult Modifier(int id)
        {
            Locaux local = _localRepository.GetLocal(id);

            return View(local);
        }

        /// <summary>
        /// Fonction qui permet de modifier un local
        /// </summary>
        /// <param name="local"></param>
        /// <returns></returns>
        [HttpPost]
        public RedirectToActionResult Modifier(Locaux local)
        {
            _localRepository.Modifier(local);
            return RedirectToAction(nameof(Liste));
        }

        /// <summary>
        /// Fonction qui permet de supprimer 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public RedirectToActionResult Supprimer(int id)
        {
            _localRepository.Supprimer(id);
            return RedirectToAction(nameof(Liste));
        }
    }
}
