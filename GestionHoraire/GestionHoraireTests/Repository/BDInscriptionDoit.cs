﻿//Author : Jean-François Gamache
using GestionHoraire.Migrations;
using GestionHoraire.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace GestionHoraireTests.Repository
{
    public class BDInscriptionDoit //BDPanier
    {
        // Initialisation des variables globales pour la classe de test
        private DbContextOptions<GestionHoraireDBContext> _options;
        private GestionHoraireDBContext _dbContext;
        private BDPanierRepository _panierRepositiory;
        private BDClientRepository _clientRepository;
        private BDCoursRepository _coursRepository;

        public BDInscriptionDoit()
        {
            // Configuration des options pour la base de données en mémoire
            _options = new DbContextOptionsBuilder<GestionHoraireDBContext>()
                .UseInMemoryDatabase("GestionHoraireDBContextTest")
                .EnableSensitiveDataLogging()
                .Options;

            _dbContext = new GestionHoraireDBContext(_options);
            _panierRepositiory = new BDPanierRepository(_dbContext);
            _clientRepository = new BDClientRepository(_dbContext);
            _coursRepository = new BDCoursRepository(_dbContext);
        }

        // Test de l'ajout d'une inscription
        [Fact]
        public void TesterNouvelleInscription()
        {
            Cours cours = new Cours
            {
                Id = 500,
                Code = "111-111-AA",
                Nom = "Test",
                Ponderation = "1-1-1",
                Bloc = "1-1",
                Prix = 60,
                Local = new Locaux { Id = 1, No_local = "1.085", Capacite = 30 },
                Enseignant = new Enseignants { Id = 1, Prenom = "Dalicia", Nom = "Bouallouche", Bureau = "1.075", Poste = 2325 }
            };
            _dbContext.Cours.Add(cours);
            Clients client = new Clients
            {
                Id = 500,
                UserID = "500",
                Nom = "test",
                Prenom = "testprenom"
            };
            _dbContext.Clients.Add(client);
            _dbContext.SaveChanges();

            //Ajout de l'inscription via le repository
            _panierRepositiory.AjouterAuPanier(cours, client);

            //Vérification que l'inscription a bien été ajouté
            Assert.Contains(_panierRepositiory.GetInscription(500), i => i.Id == 500);
            Assert.NotEmpty(_panierRepositiory.GetInscription(500));

            //Ajouter d'une nouvelle inscription pour le même client
            Cours cours2 = new Cours
            {
                Id = 5,
                Code = "222-222-BB",
                Nom = "Test",
                Ponderation = "2-2-2",
                Bloc = "2-2",
                Prix = 50,
                Local = new Locaux { Id = 5, No_local = "1.085", Capacite = 30 },
                Enseignant = new Enseignants { Id = 5, Prenom = "Dalicia", Nom = "Bouallouche", Bureau = "1.075", Poste = 2325 }
            };
            _dbContext.Cours.Add(cours2);
            _dbContext.SaveChanges();
            _panierRepositiory.AjouterAuPanier(cours2, client);

            // Vérification que la nouvelle inscription a bien été ajouté
            Assert.Equal(2, _panierRepositiory.GetInscription(500).Count());
        }

        [Fact]
        public void TestSupprimerInscription()
        {
            //Ajouter des cours et des clients au Db
            Cours cours = new Cours
            {
                Id = 700,
                Code = "111-111-AA",
                Nom = "Test",
                Ponderation = "1-1-1",
                Bloc = "1-1",
                Prix = 60,
                Local = new Locaux { Id = 1, No_local = "1.085", Capacite = 30 },
                Enseignant = new Enseignants { Id = 1, Prenom = "Dalicia", Nom = "Bouallouche", Bureau = "1.075", Poste = 2325 }
            };
            _dbContext.Cours.Add(cours);

            Cours cours2 = new Cours
            {
                Id = 5,
                Code = "222-222-BB",
                Nom = "Test",
                Ponderation = "2-2-2",
                Bloc = "2-2",
                Prix = 50,
                Local = new Locaux { Id = 5, No_local = "1.085", Capacite = 30 },
                Enseignant = new Enseignants { Id = 5, Prenom = "Dalicia", Nom = "Bouallouche", Bureau = "1.075", Poste = 2325 }
            };
            _dbContext.Cours.Add(cours2);

            Clients client = new Clients
            {
                Id = 700,
                UserID = "700",
                Nom = "test",
                Prenom = "testprenom"
            };
            _dbContext.Clients.Add(client);
            _dbContext.SaveChanges();

            //ajouter les inscriptions
            _panierRepositiory.AjouterAuPanier(cours, client);
            _panierRepositiory.AjouterAuPanier(cours2, client);

            //Supprimer 
            _panierRepositiory.SupprimerDuPanier(cours, client);

            Assert.Equal(1, _panierRepositiory.GetInscription(700).Count());
        }

        [Fact]
        public void TestViderInscription()
        {

            //Ajouter des cours et des clients au Db
            Cours cours = new Cours
            {
                Id = 700,
                Code = "111-111-AA",
                Nom = "Test",
                Ponderation = "1-1-1",
                Bloc = "1-1",
                Prix = 60,
                Local = new Locaux { Id = 1, No_local = "1.085", Capacite = 30 },
                Enseignant = new Enseignants { Id = 1, Prenom = "Dalicia", Nom = "Bouallouche", Bureau = "1.075", Poste = 2325 }
            };
            _dbContext.Cours.Add(cours);

            Cours cours2 = new Cours
            {
                Id = 5,
                Code = "222-222-BB",
                Nom = "Test",
                Ponderation = "2-2-2",
                Bloc = "2-2",
                Prix = 50,
                Local = new Locaux { Id = 5, No_local = "1.085", Capacite = 30 },
                Enseignant = new Enseignants { Id = 5, Prenom = "Dalicia", Nom = "Bouallouche", Bureau = "1.075", Poste = 2325 }
            };
            _dbContext.Cours.Add(cours2);

            Clients client = new Clients
            {
                Id = 700,
                UserID = "700",
                Nom = "test",
                Prenom = "testprenom"
            };
            _dbContext.Clients.Add(client);
            _dbContext.SaveChanges();

            //ajouter les inscriptions
            _panierRepositiory.AjouterAuPanier(cours, client);
            _panierRepositiory.AjouterAuPanier(cours2, client);

            //Vider le panier
            _panierRepositiory.ViderPanier(client);

            Assert.Null(_panierRepositiory.GetInscription(500));
        }
    }
}
