﻿//Author: JeanFrancois Gamache et Samir.el
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GestionHoraire.Models
{
    public class InscriptionPanier
    {
        public int Id { get; set; }
        public int CoursId { get; set; }
        public Cours Cours { get; set; }
        public int ClientId { get; set; }
        public Clients Client { get; set; }
    }
}
