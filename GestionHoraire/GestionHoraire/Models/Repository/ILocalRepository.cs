﻿//Author: Jean-Francois Gamache
namespace GestionHoraire.Models
{
    public interface ILocalRepository
    {
        public List<Locaux> ListeLocaux { get; }

        public void Ajouter(Locaux local);

        public void Modifier(Locaux local);

        public void Supprimer(int id);

        public Locaux GetLocal(int id);
    }
}
