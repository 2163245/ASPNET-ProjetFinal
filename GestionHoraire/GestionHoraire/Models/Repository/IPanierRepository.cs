﻿//Author Samir El Haddaji
namespace GestionHoraire.Models;

public interface IPanierRepository
{
    public List<InscriptionPanier> InscriptionPanier { get; }
    public List<Cours> GetInscription(int id);
    public bool AjouterAuPanier(Cours cours, Clients client);
    void SupprimerDuPanier(Cours cours, Clients client);
    public void ViderPanier(Clients client);
}
