﻿//Author: Jean-Francois Gamache
namespace GestionHoraire.Models
{
    public interface ICoursRepository
    {
        public List<Cours> ListeCours { get; }

        public void Ajouter(Cours cours);

        public void Modifier(Cours cours);

        public void Supprimer(int id);

        public Cours GetCours(int id);

        public List<Clients> GetListeClients(int id);

        public (Cours, List<Clients>) GetCoursClient(int id);
    }
}
