﻿//Samir Et JeanFrancois, Inspirée des notes de cours
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Diagnostics;
using GestionHoraire.ViewModels;

namespace GestionExceptions.Controllers
{
    public class ErreursController : Controller
    {
        [Route("Erreurs/Status{codeEtat}")]
        public IActionResult GererErreur(int codeEtat)
        {
            ErreurViewModel erreurVM = new ErreurViewModel
            {
                CodeErreur = codeEtat
            };

            if(codeEtat == 404)
            {
                erreurVM.MessagePersonnalise = "Cette page n'existe pas...";
            }

            return View("Erreur", erreurVM);
        }

        [Route("/ErreurGlobal")]
        public ViewResult ErreurGlobal()
        {
            //Ajouter message d'erreur pour developpeur
            IExceptionHandlerPathFeature exceptionDetails = HttpContext.Features.Get<IExceptionHandlerPathFeature>();
            string hostingEnv = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            ErreurViewModel erreurVm = new ErreurViewModel
            {
                CodeErreur = HttpContext.Response.StatusCode,
                MessagePersonnalise = "Oops, une erreure s'est produite!",

                CheminException = exceptionDetails.Path,
                MessageException = exceptionDetails.Error.Message,
                TraceException = exceptionDetails.Error.StackTrace,
                ModeDev = hostingEnv == "Development"
            };

            return View("Erreur", erreurVm);
        }
    }
}
