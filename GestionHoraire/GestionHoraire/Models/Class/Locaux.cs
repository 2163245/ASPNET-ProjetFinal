﻿//Author Jean-Francois Gamache
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GestionHoraire.Models
{
    public class Locaux
    {
        [DisplayName("Identifiant")]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [DisplayName("Numéro du local")]
        [Required(ErrorMessage = "Le champ {0} est requis")]

        public string No_local { get; set; }

        [DisplayName("Capacité")]
        [Required(ErrorMessage = "Le champ {0} est requis")]

        public int Capacite { get; set; }
    }
}
