﻿//Auteur: Jean-Francois Gamache
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;


namespace GestionHoraire.Models
{
    public class BDCoursRepository : ICoursRepository
    {
        //Initalisé le DBContext
        private readonly GestionHoraireDBContext _DBContext;

        public BDCoursRepository(GestionHoraireDBContext dBContext)
        {
            _DBContext = dBContext;
        }

        //Méthodes
        public List<Cours> ListeCours
        {
            get => _DBContext.Cours.Include(e => e.Enseignant).OrderBy(c => c.Id).ToList();
        }

        public void Ajouter(Cours cours)
        {
            //Récupérer l'enseignant et le local
            Enseignants enseignant = _DBContext.Enseignant.FirstOrDefault(e => e.Id == cours.EnseignantId);
            cours.Enseignant = enseignant;

            Locaux local = _DBContext.Locaux.FirstOrDefault(l => l.Id == cours.LocalId);
            cours.Local = local;
            _DBContext.Cours.Add(cours);
            _DBContext.SaveChanges();
        }

        public Cours GetCours(int id)
        {
            Cours cours = _DBContext.Cours.Include(c => c.Enseignant).FirstOrDefault(c => c.Id == id);
            return cours;
        }

        /// <summary>
        /// Trouve et retourne le cours et la liste des clients qui sont associé à ce cours. 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public (Cours, List<Clients>) GetCoursClient(int id)
        {
            Cours cours = _DBContext.Cours
            .Include(c => c.Enseignant)
            .Include(c => c.ListeClients)
            .ThenInclude(i => i.Client)
            .FirstOrDefault(c => c.Id == id);

            List<Clients> clients = cours?.ListeClients.Select(i => i.Client).ToList();

            return (cours, clients);
        }
        /// <summary>
        /// Fonction qui récupère tout les clients pour un cours donné
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<Clients> GetListeClients(int id)
        {
            Cours cours = _DBContext.Cours
           .Include(c => c.Enseignant)
           .Include(c => c.ListeClients)
           .ThenInclude(i => i.Client)
           .FirstOrDefault(c => c.Id == id);

            List<Clients> ListeClients = cours?.ListeClients.Select(i => i.Client).ToList();
            return ListeClients;
        }
        /// <summary>
        /// Fonction qui permet de modifier un cours
        /// </summary>
        /// <param name="cours"></param>
        public void Modifier(Cours cours)
        {
            //Récupérer l'enseignant et le local
            Enseignants enseignant = _DBContext.Enseignant.FirstOrDefault(e => e.Id == cours.EnseignantId);
            cours.Enseignant = enseignant;

            Locaux local = _DBContext.Locaux.FirstOrDefault(l => l.Id == cours.LocalId);
            cours.Local = local;

            List<Clients> listeClients = JsonConvert.DeserializeObject<List<Clients>>(cours.ListeClientsJson);

            _DBContext.Cours.Update(cours);
            _DBContext.SaveChanges();
        }

        /// <summary>
        /// Trouver le cours équivalent à l'ID et le supprimer de la base de donné
        /// </summary>
        /// <param name="id"></param>
        public void Supprimer(int id)
        {
            Cours cours = _DBContext.Cours.FirstOrDefault(c => c.Id == id);
            if (cours != null)
            {
                _DBContext.Cours.Remove(cours);
                _DBContext.SaveChanges();
            }
        }
    }
}