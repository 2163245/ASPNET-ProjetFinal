﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GestionHoraire.Migrations
{
    public partial class Inscription : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Clients_Cours_CoursId",
                table: "Clients");

            migrationBuilder.DropIndex(
                name: "IX_Clients_CoursId",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "CoursId",
                table: "Clients");

            migrationBuilder.CreateTable(
                name: "Inscription",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CoursId = table.Column<int>(type: "int", nullable: false),
                    ClientId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Inscription", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Inscription_Clients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Inscription_Cours_CoursId",
                        column: x => x.CoursId,
                        principalTable: "Cours",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Inscription_ClientId",
                table: "Inscription",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_Inscription_CoursId",
                table: "Inscription",
                column: "CoursId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Inscription");

            migrationBuilder.AddColumn<int>(
                name: "CoursId",
                table: "Clients",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Clients_CoursId",
                table: "Clients",
                column: "CoursId");

            migrationBuilder.AddForeignKey(
                name: "FK_Clients_Cours_CoursId",
                table: "Clients",
                column: "CoursId",
                principalTable: "Cours",
                principalColumn: "Id");
        }
    }
}
