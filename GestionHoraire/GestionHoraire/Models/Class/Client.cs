﻿using GestionHoraire.Models;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel;

namespace GestionHoraire.Models
{
    public class Clients
    {
        public int Id { get; set; }
        public string UserID { get; set; }
        [DisplayName("Prenom")]
        public string Prenom { get; set; }
        [DisplayName("Nom")]
        public string Nom { get; set; }
        [DisplayName("Liste des cours")]
        public ICollection<InscriptionPanier> ListeCours { get; set; }
    }
}