﻿//@Author: samir
using Microsoft.AspNetCore.Mvc;
using GestionHoraire.Models;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace GestionHoraire.Controllers
{
    public class ClientController : Controller
    {
        IClientRepository _clientRepository;

        public ClientController(IClientRepository clientRepository)
        {
            _clientRepository = clientRepository;
        }

        [HttpGet]
        public ViewResult Connexion()
        {
            ViewData["Title"] = "Connexion";
            return View();
        }

        [HttpGet]
        public ViewResult Inscription()
        {
            ViewData["Title"] = "Inscription";
            return View();
        }

        [HttpGet]
        public ViewResult Ajouter()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Ajouter(Clients client)
        {
            _clientRepository.Ajouter(client);
            return RedirectToAction("Index", "Accueil");
        }

        [HttpGet]
        public ViewResult Details()
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            Clients client = _clientRepository.ListeClients.FirstOrDefault(c => c.UserID == userId);
            return View(client);
        }
    }
}