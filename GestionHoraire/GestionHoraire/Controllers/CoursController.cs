﻿//Author Jean-Francois Gamache
using GestionHoraire.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;

namespace GestionHoraire.Controllers
{
    [Authorize]
    public class CoursController : Controller
    {
        private ICoursRepository _coursRepository;
        IEnseignantRepository _enseignantRepository;
        IClientRepository _clientRepository;
        ILocalRepository _localRepository;
        public CoursController(ICoursRepository coursRepository, IEnseignantRepository enseignantRepository, IClientRepository clientRepository, ILocalRepository localRepository)
        {
            _coursRepository = coursRepository;
            _enseignantRepository = enseignantRepository;
            _clientRepository = clientRepository;
            _localRepository = localRepository;
        }
        /// <summary>
        /// Fonction qui envoie la liste des cours
        /// </summary>
        /// <returns></returns>
        public ViewResult Liste()
        {
            ViewData["Title"] = "Liste des cours";
            return View(_coursRepository.ListeCours);
        }
        /// <summary>
        /// Fonction qui prend le Id d'un cours en paramètres et qui envoie à une page qui 
        /// montre les détails pour se cours
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ViewResult Details(int id)
        {
            var cours = _coursRepository.GetCoursClient(id);
            return View(cours);
        }

        /// <summary>
        /// Fonction qui permet d'apporter à la page d'ajout d'un cours
        /// Envoie la liste des enseignants et locaux pour les insérés dans un select
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ViewResult Ajouter()
        {
            ViewBag.Enseignants = _enseignantRepository.ListeEnsignant.Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Prenom + ' ' + x.Nom }).ToList();
            ViewBag.Locaux = _localRepository.ListeLocaux.Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.No_local}).ToList();
            return View();
        }
        /// <summary>
        /// Fonction qui permet de rajouter un cours à la liste des cours
        /// </summary>
        /// <param name="cours"></param>
        /// <returns></returns>
        [HttpPost]
        public RedirectToActionResult Ajouter(Cours cours)
        {
            _coursRepository.Ajouter(cours);
            return RedirectToAction(nameof(Liste));
        }

        /// <summary>
        /// Fonction qui apporte a la page pour modifier
        /// Prend en parametre le id pour récupérer les informations du cours présent
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ViewResult Modifier(int id)
        {
            Cours cours = _coursRepository.GetCours(id);

            //Source: ChatGPT (1) -> Voir en bas de la page
            //Récupérer la liste des Enseignants sauf l'enseignant dans le cours qu'on modifie (Il est placé dans le form)
            ViewBag.Enseignants = _enseignantRepository.ListeEnsignant
                .Where(e => e.Id != cours.Enseignant.Id)
                .Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Prenom + ' ' + x.Nom }).ToList();

            //Récupérer la liste des locaux sauf le local dans le cours qu'on modifie (Il est placé dans le form)
            ViewBag.Locaux = _localRepository.ListeLocaux
               .Where(l => l.Id != cours.Local.Id)
               .Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.No_local}).ToList();

            //Récupérer la liste des clients
            List<Clients> listeClients = _coursRepository.GetListeClients(id);
            //Source ChatGPT (2): Voir bas de la page
            ViewBag.ListeClients = JsonConvert.SerializeObject(listeClients, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });

            return View(cours);
        }
        /// <summary>
        /// Fonctuion qui permet de modifier le cours de la liste et rapporter à la liste des cours
        /// </summary>
        /// <param name="cours"></param>
        /// <returns></returns>
        [HttpPost]
        public RedirectToActionResult Modifier(Cours cours)
        {
            _coursRepository.Modifier(cours);
            return RedirectToAction(nameof(Liste));
        }

        /// <summary>
        /// Fonction qui permet de supprimer un cours de la liste.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public RedirectToActionResult Supprimer(int id)
        {
            _coursRepository.Supprimer(id);
            return RedirectToAction(nameof(Liste));
        }
    }
}

//Source ChatGPT dans Modifier GET
//Question: (Était une série de petite question pour savoir comment sélectionner l'enseignant du Cours pour l'afficher en premier dans une Select Html.

/* User
I am selecting my Enseignant this way in my form to modify my object: <div class="pb-3">
         <label asp-for="Enseignant" class="form-label">Sélectionner l'enseignant</label>
        <div class="col-sm-10">
            <select asp-for="Enseignant" asp-items="ViewBag.Enseignants" class="form-select">
                <option value="1">@Model.Enseignant.Prenom @Model.Enseignant.Nom</option>
            </select>
        </div>
    </div> The problem is that it will have the Enseignant twice in that List. Is there a way for me to remove it by searching with the name?  
 */
/* 
 Yes, you can remove the duplicated item from the dropdown list by filtering the `ViewBag.Enseignants` list based on the name. You can modify the code as follows:

```html
<div class="pb-3">
    <label asp-for="Enseignant" class="form-label">Sélectionner l'enseignant</label>
    <div class="col-sm-10">
        <select asp-for="Enseignant" asp-items="@(new SelectList(ViewBag.Enseignants.Where(e => $"{e.Prenom} {e.Nom}" != $"{Model.Enseignant.Prenom} {Model.Enseignant.Nom}"), "Id", "PrenomNom"))" class="form-select">
            <option value="@Model.Enseignant.Id">@Model.Enseignant.Prenom @Model.Enseignant.Nom</option>
        </select>
    </div>
</div>
```

In this code, the `ViewBag.Enseignants` list is filtered using the `Where` method to exclude the item that matches the name of the current `Enseignant` in the `Model`. The filtered list is then passed to the `SelectList` constructor to create the dropdown list.

Make sure to adjust the property names (`Id`, `Prenom`, `Nom`) in the `SelectList` constructor based on the properties of the `Enseignant` class in your application.

By applying this change, the dropdown list will exclude the current `Enseignant` from the options, preventing duplication.

I hope this helps! Let me know if you have any further questions.
 */

//Source ChatGPT Dans modifier (2)
//
/*Question: I have a list of clients (ListeClients) I verified that the list isn't empty when I send it to this page: <!-- Author Jean-Francois Gamache -->
@model Cours


<h1 class="text-white text-center">Ajouter un nouveau cours</h1>

<div class="row text-white m-4">
    <form asp-controller="Cours" asp-action="Modifier" method="post">
        <input asp-for="Id" value="@Model.Id" type=hidden />
    <div class="pb-2 form-group">
            <label asp-for="Code" class="form-label">Code</label>
            <input asp-for="Code" class="form-control" type="text" minlength="4" maxlength="50" placeholder="Veuillez entrer le code du cours" value="@Model.Code">
            <span asp-validation-for="Code" class="text-danger"></span>
    </div>

    <div class="pb-3">
            <label asp-for="Nom" class="form-label">Nom</label>
            <input asp-for="Nom" class="form-control" type="text" minlength="4" maxlength="50" placeholder="Veuillez entrer le nom du cours" value="@Model.Nom">
            <span asp-validation-for="Nom" class="text-danger"></span>
    </div>

    <div class="pb-3">
            <label asp-for="Ponderation" class="form-label">Ponderation</label>
            <input asp-for="Ponderation" class="form-control" type="text" minlength="2" maxlength="6" placeholder="Veuillez entrer la ponderation du cours" value="@Model.Ponderation">
            <span asp-validation-for="Ponderation" class="text-danger"></span>
    </div>

    <div class="pb-3">
            <label asp-for="Bloc" class="form-label">Bloc</label>
            <input asp-for="Bloc" class="form-control" type="text" minlength="1" maxlength="5" placeholder="Veuillez entrer le bloc du cours" value="@Model.Bloc">
            <span asp-validation-for="Bloc" class="text-danger"></span>
    </div>

    <div class="pb-3">
            <label asp-for="Prix" class="form-label">Prix</label>
            <input asp-for="Prix" class="form-control" type="number" min="5" max="360" placeholder="Veuillez entrer le prix du cours" value="@((int)Model.Prix)">
            <span asp-validation-for="Prix" class="text-danger"></span>
        </div>

    <div class="pb-3">
            <label asp-for="LocalId" class="form-label">Sélectionner le local</label>
        <div class="col-sm-10">
                <select asp-for="LocalId" asp-items="ViewBag.Locaux" class="form-select">
                <option value="1">­@Model.Local.No_local</option>
            </select>
        </div>
    </div>

    <div class="pb-3">
            <label asp-for="EnseignantId" class="form-label">Sélectionner l'enseignant</label>
        <div class="col-sm-10">
                <select asp-for="EnseignantId" asp-items="ViewBag.Enseignants" class="form-select">
                <option value="1">@Model.Enseignant.Prenom @Model.Enseignant.Nom</option>
            </select>
        </div>
    </div>

    <input asp-for="ListeClients" value="@ViewBag.ListeClients" type="hidden" />

    <button type="submit" class="btn btn-primary btn-block">Enregistrer</button>
    <a asp-action="Liste" asp-controller="Cours" class="btn btn-secondary pt-2">Annuler</a>
    </form>
    @section JQueryValidationScripts {
        <partial name="_ValidationScriptsPartial" />
    }
</div> 
But when I submit the form, the list is null. How can I make it that when I upload the form the list is still in the object. note that the user can't change that list here so the list will always stay the same after submittting the form.  
 * */

/* Reponse: 
 * If you want to preserve the `ListeClients` property value when submitting the form, you can store it as a hidden field in your form. However, since `ListeClients` is a complex object (a list), you need to convert it to a string representation, such as JSON, before storing it as a hidden field.

Here's an example of how you can achieve this:

1. Convert the `ListeClients` property to a JSON string in your controller before passing it to the view:
   ```csharp
   ViewBag.ListeClients = JsonConvert.SerializeObject(ListeClients);
   ```

2. In your view, store the serialized `ListeClients` as a hidden field:
   ```html
   <input asp-for="ListeClients" value="@ViewBag.ListeClients" type="hidden" />
   ```

3. When submitting the form, retrieve the serialized `ListeClients` from the form data and deserialize it back to a list in your controller action:
   ```csharp
   [HttpPost]
   public IActionResult Modifier(Cours cours)
   {
       string listeClientsJson = Request.Form["ListeClients"];
       List<Client> listeClients = JsonConvert.DeserializeObject<List<Client>>(listeClientsJson);

       // Use the `listeClients` in your logic

       // Rest of your code
   }
   ```

By serializing the `ListeClients` to a JSON string and storing it as a hidden field, you can retrieve it back in your controller action and deserialize it to obtain the original list.

Make sure to include the necessary `using` statements at the top of your controller file:
```csharp
using Newtonsoft.Json;
using System.Collections.Generic;
```

With these changes, the `ListeClients` property should retain its value when submitting the form.
 */