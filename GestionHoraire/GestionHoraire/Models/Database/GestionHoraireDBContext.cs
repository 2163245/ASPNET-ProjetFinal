﻿using Microsoft.EntityFrameworkCore;
using GestionHoraire.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace GestionHoraire.Models
{
    public class GestionHoraireDBContext : IdentityDbContext
    {
        public GestionHoraireDBContext(DbContextOptions<GestionHoraireDBContext> options) : base(options)
        {
            
        }

        public DbSet<Clients> Clients { get; set; }
        public DbSet<Cours> Cours { get; set; }
        public DbSet<Enseignants> Enseignant { get; set; }
        public DbSet<Locaux> Locaux { get; set; }
        public DbSet<InscriptionPanier> Inscription { get; set; }
    }
}
