﻿//Author : Samir El Haddaji
using GestionHoraire.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionHoraireTests.Repository
{
    public class BDLocalRepositoryDoit
    {
        // Initialisation des variables globales pour la classe de test
        private DbContextOptions<GestionHoraireDBContext> _options;
        private GestionHoraireDBContext _dbContext;
        private BDLocalRepository _localRepository;

        public BDLocalRepositoryDoit()
        {
            // Configuration des options pour la base de données en mémoire
            _options = new DbContextOptionsBuilder<GestionHoraireDBContext>()
                .UseInMemoryDatabase("GestionHoraireDBContextTest")
                .Options;

            _dbContext = new GestionHoraireDBContext(_options);
            _localRepository = new BDLocalRepository(_dbContext);
        }

        // Test de la création d'un nouveau local
        [Fact]
        public void TesterAjouterLocal()
        {
            var local = new Locaux { Id = 1, No_local = "1.085", Capacite = 30 };

            // Ajout du local via le repository
            _localRepository.Ajouter(local);

            // Vérification que le local a bien été ajouté
            Assert.NotEmpty(_localRepository.ListeLocaux);
            Assert.Equal(1, _localRepository.ListeLocaux.Count);
            Assert.Contains(_localRepository.ListeLocaux, l => l.Id == 1);
        }

        // Test de la modification d'un local
        [Fact]
        public void TesterModifierLocal()
        {
            var local = new Locaux { Id = 2, No_local = "1.085", Capacite = 30 };

            // Ajout de du local directement dans le contexte de la base de données
            _dbContext.Locaux.Add(local);
            _dbContext.SaveChanges();

            // Modification d'une propriété du local
            local.No_local = "1.111";

            // Mise à jour du local via le repository
            _localRepository.Modifier(local);

            // Vérification que le local a bien été modifié
            Assert.Equal("1.111", _localRepository.GetLocal(2).No_local);
        }

        // Test de la suppression d'un local
        [Fact]
        public void TesterSupprimerLocal()
        {
            var local = new Locaux { Id = 3, No_local = "1.085", Capacite = 30 };

            // Ajout de du local directement dans le contexte de la base de données
            _dbContext.Locaux.Add(local);
            _dbContext.SaveChanges();

            // Suppression du local via le repository
            _localRepository.Supprimer(3);

            // Vérification que le local a bien été supprimé
            Assert.Null(_localRepository.GetLocal(3));
        }
    }
}
