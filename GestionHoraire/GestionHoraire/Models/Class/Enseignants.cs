﻿//Author Samir El Haddaji
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GestionHoraire.Models
{
    public class Enseignants
    {
        [DisplayName("Identifiant")]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [DisplayName("Prenom")]
        [Required(ErrorMessage = "Le champ {0} est requis")]
        [RegularExpression("^[a-zA-Z]*$", ErrorMessage = "Le champ {0} doit contenir uniquement des lettres.")]
        public string Prenom { get; set; }

        [DisplayName("Nom")]
        [Required(ErrorMessage = "Le champ {0} est requis")]
        [RegularExpression("^[a-zA-Z]*$", ErrorMessage = "Le champ {0} doit contenir uniquement des lettres.")]
        public string Nom { get; set; }

        [DisplayName("Bureau")]
        [Required(ErrorMessage = "Le champ {0} est requis")]
        [Remote("ValiderBureauLocal", "Enseignant", HttpMethod = "Post")]
        public string Bureau { get; set; }

        [DisplayName("Poste")]
        [Required(ErrorMessage = "Le champ {0} est requis")]
        [RegularExpression("^[0-9]{1,4}$", ErrorMessage = "Le poste doit être un nombre entre 0 et 9999")]
        public int Poste { get; set; }
    }
}
