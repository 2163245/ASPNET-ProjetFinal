﻿//Author Samir El Haddaji
using GestionHoraire.Models;
using Microsoft.AspNetCore.Server.HttpSys;
using System.ComponentModel;

public class PanierViewModel
{
    public Clients Client { get; set; }
    public List<Cours> ListeCours { get; set; }
    public decimal MontantTotalPanier { get; set; }
}