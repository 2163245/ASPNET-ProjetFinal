﻿//Author : Samir El Haddaji
using GestionHoraire.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionHoraireTests.Repository
{
    public class BDCoursRepositoryDoit
    {
        // Initialisation des variables globales pour la classe de test
        private DbContextOptions<GestionHoraireDBContext> _options;
        private GestionHoraireDBContext _dbContext;
        private BDCoursRepository _coursRepository;

        public BDCoursRepositoryDoit()
        {
            // Configuration des options pour la base de données en mémoire
            _options = new DbContextOptionsBuilder<GestionHoraireDBContext>()
                .UseInMemoryDatabase("GestionHoraireDBContextTest1")
                .Options;

            _dbContext = new GestionHoraireDBContext(_options);
            _coursRepository = new BDCoursRepository(_dbContext);
        }

        // Test de la création d'un nouveau cours
        [Fact]
        public void TesterAjouterCours()
        {
            var cours = new Cours {
                Id = 1,
                Code = "111-111-AA",
                Nom = "Test",
                Ponderation = "1-1-1",
                Bloc = "1-1",
                Prix = 60,
                Local = new Locaux { Id = 1, No_local = "1.085", Capacite = 30 },
                Enseignant = new Enseignants {Id = 1, Prenom = "Dalicia", Nom = "Bouallouche", Bureau = "1.075", Poste = 2325 }
            };

            // Ajout du cours via le repository
            _coursRepository.Ajouter(cours);

            // Vérification que le cours a bien été ajouté
            Assert.NotEmpty(_coursRepository.ListeCours);
            Assert.Equal(1, _coursRepository.ListeCours.Count);
            Assert.Contains(_coursRepository.ListeCours, c => c.Id == 1);
        }

        // Test de la modification d'un cours
        [Fact]
        public void TesterModifierCours()
        {
            var cours = new Cours
            {
                Id = 2,
                Code = "222-222-BB",
                Nom = "Test2",
                Ponderation = "2-2-2",
                Bloc = "1-1",
                Prix = 60,
                Local = new Locaux { Id = 1, No_local = "1.085", Capacite = 30 },
                Enseignant = new Enseignants { Id = 1, Prenom = "Dalicia", Nom = "Bouallouche", Bureau = "1.075", Poste = 2325 }
            };

            // Ajout du cours directement dans la base de données
            _dbContext.Cours.Add(cours);
            _dbContext.SaveChanges();

            // Modification d'une propriété du cours
            cours.Nom = "CoursModifié";

            // Mise à jour du cours via le repository
            _coursRepository.Modifier(cours);

            // Vérification que le cours a bien été modifié
            Assert.Equal("CoursModifié", _coursRepository.GetCours(2).Nom);
        }

        // Test de la suppression d'un cours
        [Fact]
        public void TesterSupprimerCours()
        {
            var cours = new Cours
            {
                Id = 3,
                Code = "333-333-CC",
                Nom = "Test3",
                Ponderation = "3-3-3",
                Bloc = "1-1",
                Prix = 60,
                Local = new Locaux { Id = 1, No_local = "1.085", Capacite = 30 },
                Enseignant = new Enseignants { Id = 1, Prenom = "Dalicia", Nom = "Bouallouche", Bureau = "1.075", Poste = 2325 }
            };

            // Ajout du cours directement dans le contexte de la base de données
            _dbContext.Cours.Add(cours);
            _dbContext.SaveChanges();

            // Suppression du cours via le repository
            _coursRepository.Supprimer(3);

            // Vérification que le cours a bien été supprimé
            Assert.Null(_coursRepository.GetCours(3));
        }
    }
}
