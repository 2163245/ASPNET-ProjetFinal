﻿//Auteur: Samir El Haddaji
namespace GestionHoraire.Models
{
    public class BDClientRepository : IClientRepository
    {
        //Initalisé le DBContext
        private readonly GestionHoraireDBContext _gestionHoraireDBContext;
        public BDClientRepository(GestionHoraireDBContext gestionHoraireDBContext)
        {
            _gestionHoraireDBContext = gestionHoraireDBContext;
        }
        public List<Clients> ListeClients
        {
            get => _gestionHoraireDBContext.Clients.OrderBy(c => c.Nom).ToList();
        }

        //Méthodes
        public void Ajouter(Clients client)
        {
            _gestionHoraireDBContext.Clients.Add(client);
            _gestionHoraireDBContext.SaveChanges();
        }

        public void Modifier(Clients client)
        {
            _gestionHoraireDBContext.Clients.Update(client);
            _gestionHoraireDBContext.SaveChanges();
        }

        /// <summary>
        /// Trouver le cours équivalent à l'ID et le supprimer de la base de donné
        /// </summary>
        /// <param name="id"></param>
        public void Supprimer(int id)
        {
            Clients client = _gestionHoraireDBContext.Clients.FirstOrDefault(c => c.Id == id);
            if (client != null)
            {
                _gestionHoraireDBContext.Clients.Remove(client);
                _gestionHoraireDBContext.SaveChanges();
            }
        }
    }
}
