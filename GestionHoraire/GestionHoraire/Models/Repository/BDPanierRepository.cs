﻿//Author JeanFrancois Gamache

using Microsoft.EntityFrameworkCore;

namespace GestionHoraire.Models
{
    public class BDPanierRepository : IPanierRepository
    {
        public string? PanierSessionId;
        private List<InscriptionPanier> items = new List<InscriptionPanier>();

        public List<InscriptionPanier> InscriptionPanier => items;

        //Initalisé le DBContext
        private readonly GestionHoraireDBContext _DBContext;

        public BDPanierRepository(GestionHoraireDBContext dBContext)
        {
            _DBContext = dBContext;
        }

        /// <summary>
        /// Fonction qui récupérer l'inscription d'un client à partir de l'id 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<Cours> GetInscription(int id)
        {
            Clients client = _DBContext.Clients
                .Include(c => c.ListeCours)
                .ThenInclude(i => i.Cours)
                .FirstOrDefault(c => c.Id == id);

            List<Cours> cours = client?.ListeCours.Select(i => i.Cours).ToList();

            return cours;
        }

        /// <summary>
        /// Fonction qui permet de rajouter une inscription (Panier)
        /// </summary>
        /// <param name="cours"></param>
        /// <param name="client"></param>
        public bool AjouterAuPanier(Cours cours, Clients client)
        {
            //Vérification qu'il n'est pas déjà inscrit,
            bool verifExist = _DBContext.Inscription.Any(i => i.CoursId == cours.Id && i.ClientId == client.Id);

            if (!verifExist)
            {   
                //Ajouter l'inscription
                InscriptionPanier inscription = new InscriptionPanier
                {
                    CoursId = cours.Id,
                    ClientId = client.Id,
                };

                _DBContext.Inscription.Add(inscription);
                _DBContext.SaveChanges();
                return true;
            }
            return false;
        }

        /// <summary>
        ///  Fonction qui permet de supprimer une inscription d'un client
        /// </summary>
        /// <param name="cours"></param>
        /// <param name="client"></param>
        public void SupprimerDuPanier(Cours cours, Clients client)
        {
            InscriptionPanier inscription = _DBContext.Inscription.FirstOrDefault(i => i.CoursId == cours.Id && i.ClientId == client.Id);
            _DBContext.Inscription.Remove(inscription);
            _DBContext.SaveChanges();
        }

        /// <summary>
        /// Fonction qui permet de vider toutes les inscriptions du client
        /// </summary>
        public void ViderPanier(Clients client)
        {
            //Récupérer toutes les inscriptions du client
            List<InscriptionPanier> inscription = _DBContext.Inscription.Where(i => i.ClientId == client.Id).ToList();
            _DBContext.Inscription.RemoveRange(inscription);
            _DBContext.SaveChanges();
        }
    }
}
