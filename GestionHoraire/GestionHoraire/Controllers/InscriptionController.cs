﻿//Author Jean-Francois Gamache (Fonctionnement), Samir El Haddaji (Cookies)
using GestionHoraire.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace GestionHoraire.Controllers
{
    [Authorize]
    public class InscriptionController : Controller
    {
        IPanierRepository _inscriptionRepository;
        ICoursRepository _coursRepository;
        IClientRepository _clientRepository;
        
        public InscriptionController(IPanierRepository panierRepository, ICoursRepository coursRepository, IClientRepository clientRepository)
        {
            _inscriptionRepository = panierRepository;
            _coursRepository = coursRepository;
            _clientRepository = clientRepository;
        }

        /// <summary>
        /// Fonction qui permet de retourner le client connecté
        /// </summary>
        /// <returns></returns>
        public Clients GetClient()
        {
            //Récupérer le client connecté
            var idClient = User.FindFirstValue(ClaimTypes.NameIdentifier);

            Clients clientConnecte = _clientRepository.ListeClients.FirstOrDefault(c => c.UserID == idClient);
            return clientConnecte;
        }

        /// <summary>
        /// Fonction qui permet de récupérer les inscriptions d'un client
        /// </summary>
        /// <returns></returns>
        public ViewResult index()
        {
            Clients clientConnecte = GetClient();
            //Récupérer les cours du clients a partir de son ID
            var client = _inscriptionRepository.GetInscription(clientConnecte.Id);

            var viewModel = new PanierViewModel
            {
                ListeCours = client,
            };
            return View(viewModel);
        }
        
        /// <summary>
        ///  Fonction qui permet de rajouter une inscription de cours associé au client:
        /// </summary>
        /// <param name="coursId"></param>
        /// <returns></returns>
        public RedirectToActionResult AjouterAuPanier(int coursId)
        {
            //Récupérer le cours et vérifier qu'il existe.
            var cours = _coursRepository.GetCours(coursId);
            if (cours == null)
            {
                return RedirectToAction(nameof(Index));
            }

            //Récupérer le client et le rajouter à la liste des inscriptions, Si succès, incrémenter le cookie.
            Clients clientConnecte = GetClient();
            if (_inscriptionRepository.AjouterAuPanier(cours, clientConnecte))
            {
                //Cookies pour l'incrémentation du compte du nombre de cours dans le panier (@Author: Samir)
                int compteur = 0;
                if (this.Request.Cookies["nbCours"] != null)
                    compteur = Convert.ToInt16(this.Request.Cookies["nbCours"]);

                compteur++;

                this.Response.Cookies.Append("nbCours", compteur.ToString());
            }
            return RedirectToAction(nameof(Index));
        }

        /// <summary>
        /// Fonction qui récupère l'ID de l'inscription et la supprime
        /// Décremente le cookie
        /// </summary>
        /// <param name="coursId"></param>
        /// <returns></returns>
        public RedirectToActionResult SupprimerDuPanier(int coursId)
        {
            var cours = _coursRepository.GetCours(coursId);
            if (cours != null)
            {
                Clients clientConnecte = GetClient();
                _inscriptionRepository.SupprimerDuPanier(cours, clientConnecte);
            }


            //Cookies pour la décrémentation du compte du nombre de cours dans le panier (@Author: Samir)
            int compteur = 0;
            if (this.Request.Cookies["nbCours"] != null)
                compteur = Convert.ToInt16(this.Request.Cookies["nbCours"]);

            compteur--;

            this.Response.Cookies.Append("nbCours", compteur.ToString());

            return RedirectToAction(nameof(Index));
        }

        /// <summary>
        /// Fonction qui permet de supprimer toutes les inscriptions d'un client
        /// Remet le compteur du cookie à 0.
        /// </summary>
        /// <returns></returns>
        public RedirectToActionResult ViderPanier()
        {
            int compteur = 0;
            this.Response.Cookies.Append("nbCours", compteur.ToString());

            Clients clientConnecte = GetClient();
            _inscriptionRepository.ViderPanier(clientConnecte);

            return RedirectToAction(nameof(Index));
        }
    }
}