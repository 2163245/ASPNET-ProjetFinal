﻿//Author: Samir El Haddaji

namespace GestionHoraire.Models
{
    public interface IClientRepository
    {
        public List<Clients> ListeClients { get; }

        public void Ajouter(Clients client);

        public void Modifier(Clients client);

        public void Supprimer(int id);
    }
}
