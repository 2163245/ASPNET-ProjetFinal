﻿//Author Jean-Francois Gamache
using GestionHoraire.Models;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GestionHoraire.Models
{
    public class Cours
    {
        [DisplayName("Identifiant")]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [DisplayName("Code")]
        [Required(ErrorMessage = "Le champ {0} est requis")]
        public string Code { get; set; }

        [DisplayName("Nom")]
        [Required(ErrorMessage = "Le champ {0} est requis")]
        public string Nom { get; set; }

        [DisplayName("Ponderation")]
        [Required(ErrorMessage = "Le champ {0} est requis")]
        [RegularExpression(@"^\d{1}-\d{1}-\d{1}$", ErrorMessage = "Le champ {0} est incorrect. Le format correct est '2-3-1'.")]
        public string Ponderation { get; set; }

        [DisplayName("Bloc")]
        [Required(ErrorMessage = "Le champ {0} est requis")]
        [RegularExpression(@"^\d{1}-\d{1}$", ErrorMessage = "Le champ {0} est incorrect. Le format correct est '1-1'.")]
        public string Bloc { get; set; }

        [DisplayName("Prix")]
        [Required(ErrorMessage = "Le champ {0} est requis")]
        public Decimal Prix { get; set; }

        [DisplayName("Local")]
        [Required(ErrorMessage = "Le champ {0} est requis")]
        public int LocalId { get; set; } 
        public Locaux Local { get; set; }

        [DisplayName("Enseignant")]
        [Required(ErrorMessage = "Le champ {0} est requis")]
        public int EnseignantId { get; set; }
        public Enseignants Enseignant { get; set; }

        [DisplayName("Liste d'étudiants")]
        public string ListeClientsJson { get; set; } = string.Empty;
        public ICollection<InscriptionPanier> ListeClients { get; set; }
    }
}
