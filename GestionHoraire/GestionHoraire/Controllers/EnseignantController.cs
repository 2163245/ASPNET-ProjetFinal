﻿//Author: Samir
using GestionHoraire.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authorization;

namespace GestionHoraire.Controllers
{
    [Authorize]
    public class EnseignantController : Controller
    {
        ILocalRepository _localRepository;
        IEnseignantRepository _enseignantRepository;
        public EnseignantController(IEnseignantRepository enseignantRepository, ILocalRepository localRepository)
        {
            _enseignantRepository = enseignantRepository;
            _localRepository = localRepository;
        }

        public ViewResult Liste()
        {
            ViewData["Title"] = "Liste des enseignants";
            return View(_enseignantRepository.ListeEnsignant);
        }

        [HttpGet]
        public ViewResult Details(int id)
        {
            Enseignants enseignant = _enseignantRepository.ListeEnsignant.FirstOrDefault(c => c.Id == id);
            return View(enseignant);
        }

        [HttpGet]
        public ViewResult Ajouter()
        {
            return View();
        }

        [HttpPost]
        public RedirectToActionResult Ajouter(Enseignants enseignant)
        {
            _enseignantRepository.Ajouter(enseignant);
            return RedirectToAction(nameof(Liste));
        }

        [HttpGet]
        public ViewResult Modifier(int id)
        {
            Enseignants enseignants = _enseignantRepository.GetEnseignant(id);

            return View(enseignants);
        }

        [HttpPost]
        public RedirectToActionResult Modifier(Enseignants enseignant)
        {
            _enseignantRepository.Modifier(enseignant);
            return RedirectToAction(nameof(Liste));
        }

        [HttpGet]
        public RedirectToActionResult Supprimer(int id)
        {
            _enseignantRepository.Supprimer(id);
            return RedirectToAction(nameof(Liste));
        }

        [HttpPost]
        public JsonResult ValiderBureauLocal(string bureau)
        {
            var local = _localRepository.ListeLocaux.FirstOrDefault(l => l.No_local == bureau);

            if (local == null)
            {
                return Json($"Le local {bureau} n'existe pas. Veuillez le créer avant de l'attribuer à l'enseignant.");
            }
            else
            {
                return Json(true);
            }
        }
    }
}
