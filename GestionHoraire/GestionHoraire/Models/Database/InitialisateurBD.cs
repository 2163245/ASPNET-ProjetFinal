﻿using GestionHoraire.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace GestionHoraire.Models
{
    public class InitialisateurBD
    {
        //Initialisation Clients @Samir
        public static List<Clients> _clients = new List<Clients>
        {
            new Clients { Prenom = "Administrateur", Nom = "Admin", UserID="123"},
            new Clients { Prenom = "Sophie", Nom = "Tremblay", UserID="1234"},
            new Clients { Prenom = "Alexandre", Nom = "Gagnon", UserID="1235"},
            new Clients { Prenom = "Luc", Nom = "Bergeron", UserID="12376"},
            new Clients { Prenom = "Caroline", Nom = "Martin", UserID="1236"},
            new Clients { Prenom = "Maxime", Nom = "Lefebvre", UserID="12354"},
            new Clients { Prenom = "Isabelle", Nom = "Girard", UserID="1233"},
            new Clients { Prenom = "David", Nom = "Roy", UserID="123324"}
        };


        //Initialisateur Enseignants @Samir
        public static List<Enseignants> _enseignants = new List<Enseignants>
        {
            new Enseignants{Prenom = "Dalicia", Nom = "Bouallouche", Bureau = "1.075", Poste = 2325},
            new Enseignants{Prenom = "Valérie", Nom = "Levasseur", Bureau = "1.075", Poste = 2017},
            new Enseignants{Prenom = "Ahmed", Nom = "Bounouar", Bureau = "1.067", Poste = 2291},
            new Enseignants{Prenom = "Maryse", Nom = "Mongeau", Bureau = "1.075A", Poste = 2023},
            new Enseignants{Prenom = "Mathieu", Nom = "Lizotte", Bureau = "2.545", Poste = 2401},
            new Enseignants{Prenom = "François", Nom = "Pagé", Bureau = "1.083", Poste = 2297},
            new Enseignants{Prenom = "Pascale", Nom = "J. Prud'homme", Bureau = "2.917", Poste = 3270},
            new Enseignants{Prenom = "Raphael", Nom = "Mayrand-St-Gelais", Bureau = "1.077", Poste = 2297}
        };


        //Initialisateur Locaux @JeanFrancois
        public static List<Locaux> _locaux = new List<Locaux>
        {
            new Locaux{No_local = "1.085", Capacite = 30},
            new Locaux{No_local = "1.348", Capacite = 25},
            new Locaux{No_local = "1.547", Capacite = 30},
            new Locaux{No_local = "2.325", Capacite = 20},
            new Locaux{No_local = "2.147", Capacite = 15},
            new Locaux{No_local = "2.125", Capacite = 30},
            new Locaux{No_local = "1.101", Capacite = 28},
        };

        //Initialisateur Cours @JeanFrancois
        public static List<Cours> _cours = new List<Cours>
        {
            new Cours{Code = "420-4P3-HU", Nom = "Développement Web en ASP.NET", Ponderation = "2-3-1", Bloc = "3-2", Prix = 60,
                Local = _locaux[0],
                Enseignant = _enseignants[0]},

            new Cours{Code = "420-4P2-HU", Nom = "Développement Web en PHP", Ponderation = "2-2-1", Bloc = "2-2", Prix = 40,
                Local = _locaux[0],
                Enseignant = _enseignants[1]},

            new Cours{Code = "420-4P1-HU", Nom = "Développement d'applications mobiles", Ponderation = "2-2-1", Bloc = "4", Prix = 40,
                Local = _locaux[1],
                Enseignant = _enseignants[3]},

            new Cours{Code = "420-4P4-HU", Nom = "Serveur et Client", Ponderation = "2-1-2", Bloc = "3-2", Prix = 60,
                Local = _locaux[2],
                Enseignant = _enseignants[2],},

            new Cours{Code = "601-EWT-HU", Nom = "Français adapté aux programmes techniques", Ponderation = "2-1-1", Bloc = "2-2", Prix = 40,
                Local = _locaux[3],
                Enseignant = _enseignants[6]},

            new Cours{Code = "360-4G0-HU", Nom = "Communication, lois et éthique", Ponderation = "3-1-3", Bloc = "2-2", Prix = 40,
                Local = _locaux[4],
                Enseignant = _enseignants[4]},

            new Cours{Code = "420-4G1-HU", Nom = "Soutien Informatique", Ponderation = "1-2-3", Bloc = "3", Prix = 50,
                Local = _locaux[5],
                Enseignant = _enseignants[5]},

            new Cours{Code = "420-4G7-HU", Nom = "Vieille technologique", Ponderation = "1-2-3", Bloc = "3", Prix = 50,
                Local = _locaux[6],
                Enseignant = _enseignants[7]},
        };

        //Seeder de l'application
        /// <summary>
        /// Ajout des données initiales à la base de données. (Client, Cours, Enseignant, Locaux) @Samir et Jean-Francois
        /// </summary>
        /// <param name="applicationBuilder"></param>
        public static async void Seed(IApplicationBuilder applicationBuilder)
        {
            GestionHoraireDBContext gestionHoraireDBContext = applicationBuilder.ApplicationServices.CreateScope().ServiceProvider.GetRequiredService<GestionHoraireDBContext>();


            if (!gestionHoraireDBContext.Cours.Any())
            {
                gestionHoraireDBContext.Cours.AddRange(_cours);
                gestionHoraireDBContext.SaveChanges();
            }

            if (!gestionHoraireDBContext.Clients.Any())
            {
                gestionHoraireDBContext.Clients.AddRange(_clients);
                gestionHoraireDBContext.SaveChanges();
            }

            if (!gestionHoraireDBContext.Enseignant.Any())
            {
                gestionHoraireDBContext.Enseignant.AddRange(_enseignants);
                gestionHoraireDBContext.SaveChanges();
            }

            if (!gestionHoraireDBContext.Locaux.Any())
            {
                gestionHoraireDBContext.Locaux.AddRange(_locaux);
                gestionHoraireDBContext.SaveChanges();
            }

            if (!gestionHoraireDBContext.Inscription.Any())
            {
                Random rnd = new Random();
                foreach (Cours cours in _cours)
                {
                    //Choisi 4 clients au hasard et prend les 4 premiers 
                    List<Clients> randomClients = _clients.OrderBy(c => rnd.Next()).ToList();
                    List<Clients> baseClients = randomClients.Take(4).ToList();

                    //Pour chaque client pris au hasard, Ajouter une inscription au cours
                    foreach (Clients client in baseClients)
                    {
                        InscriptionPanier inscription = new InscriptionPanier
                        {
                            CoursId = cours.Id,
                            ClientId = client.Id,                            
                        };
                        gestionHoraireDBContext.Inscription.Add(inscription);
                    }
                }
                gestionHoraireDBContext.SaveChanges();
            }

            //Générer les roles et utilisateurs: @Author: JeanFrancois Gamache
            //Inspirer des solutions: https://stackoverflow.com/questions/34343599/how-to-seed-users-and-roles-with-code-first-migration-using-identity-asp-net-cor
            var roleManager = applicationBuilder.ApplicationServices.GetRequiredService<RoleManager<IdentityRole>>();
            var userManager = applicationBuilder.ApplicationServices.GetRequiredService<UserManager<IdentityUser>>();
           
            //Créer les roles
            string[] roles = new string[] { "Admin", "User" };
            foreach (string role in roles)
            {
                if (!roleManager.RoleExistsAsync(role).GetAwaiter().GetResult())
                {
                    roleManager.CreateAsync(new IdentityRole(role)).GetAwaiter().GetResult();
                }
            }

            // Créer un utilisateur
            var adminEmail = "admin1@gmail.com";
            var adminPassword = "Password!";

            var adminUser = userManager.FindByEmailAsync(adminEmail).GetAwaiter().GetResult();
            if (adminUser == null)
            {
                adminUser = new IdentityUser
                {
                    Email = adminEmail,
                    UserName = "Admin",
                    PhoneNumber = "+111111111111",
                    EmailConfirmed = true,
                    PhoneNumberConfirmed = true,
                    SecurityStamp = Guid.NewGuid().ToString("D"),
                    LockoutEnabled = false,

                };
                var result = await userManager.CreateAsync(adminUser);
                if (result.Succeeded)
                {
                    await userManager.AddPasswordAsync(adminUser, adminPassword);
                    await userManager.AddToRoleAsync(adminUser, "Admin");
                }
            }

            gestionHoraireDBContext.SaveChanges();
        }
    }
}